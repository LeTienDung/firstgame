﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCube: MonoBehaviour
{
    private GameObject cube;
    public float speed;
    int t;
    void Start()
    {
        cube = gameObject;
        t = 1;
    }

    void Update()
    {
        cube.transform.position = new Vector3(cube.transform.position.x + t * speed * Time.deltaTime, cube.transform.position.y, cube.transform.position.z);

        if ( -5 >= cube.transform.position.x)
        {
            t = 1;
        }
        if ( cube.transform.position.x >= 5)
        {
            t = -1;
        }
    }

}
