﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationCubeX : MonoBehaviour
{
    private GameObject cube;
    public float speed;
    void Start()
    {
        cube = gameObject;
    }
    void Update()
    {
        cube.transform.Rotate(0, speed * Time.deltaTime, 0);   
    }
}
