﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationCubeZ : MonoBehaviour
{
    private GameObject cube;
    public float speed;
    int t;
    
    void Start()
    {
        cube = gameObject;
    }

    void Update()
    {
        cube.transform.Rotate(0, 0, speed * Time.deltaTime);
    }
}
