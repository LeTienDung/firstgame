﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplicationBarrier : MonoBehaviour
{
    // Start is called before the first frame update
    
    
    [SerializeField] bool spawn;
    public GameObject cube;
    public float resetTime;
    void Start()
    {
        
        spawn = true;
    }
    // Update is called once per frame
    void Update()
    {
            if (spawn)
            {
                spawn = false;
                Invoke("ResetTime", resetTime);
            }
        

    }
    void ResetTime()
    {
        spawn = true;
        SpawnBarrier();

    }
    public void SpawnBarrier()
    {
        

        Vector3 barrierPos = new Vector3(0, 0.2f, 80);
        Instantiate(cube, barrierPos, Quaternion.identity);

    }
}
