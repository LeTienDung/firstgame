﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Touch : MonoBehaviour
{
    public bool touched;
    void Start()
    {
        touched = false;
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        touched = true;
    }
}
