﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBarrierZ : MonoBehaviour
{
    public float moveSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.transform.position.z > -5)
            gameObject.transform.position = gameObject.transform.position + new Vector3(0, 0, moveSpeed * Time.deltaTime);
        else
            gameObject.transform.position = new Vector3(0, 0.2f, 80);

    }
}
