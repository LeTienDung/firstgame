﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public float speed;

    /*private void FixedUpdate()
    {
        float horAxis = Input.GetAxis("Horizontal");
        float verAxis = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(horAxis, 0.0f, verAxis);

        GetComponent<Rigidbody>().AddForce(movement * speed * Time.deltaTime);
    }*/


    protected Joystick joystick;
    protected JoyButton joybutton;
    protected bool jump;
    void Start()
    {
        joystick = FindObjectOfType<Joystick>();
        joybutton = FindObjectOfType<JoyButton>();
        jump = false;
    }
    void Update()
    {
        var rigidbody = GetComponent<Rigidbody>();
        rigidbody.velocity = new Vector3(joystick.Horizontal * Time.deltaTime * speed, rigidbody.velocity.y, joystick.Vertical * Time.deltaTime*speed);
        if(! jump && joybutton.Pressed)
        {
            jump = true;
            if ( gameObject.transform.position.y < 0.4f && gameObject.transform.position.x <5 && gameObject.transform.position.x > -5)
            rigidbody.velocity += Vector3.up * 6f;
        }    
        if (jump && !joybutton.Pressed)
        {
            jump = false;
        }    
    }
}
