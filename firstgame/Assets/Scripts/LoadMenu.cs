﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadMenu : MonoBehaviour
{
    
    public void Man1()
    {
        SceneManager.LoadScene(1);
    }
    public void Man2()
    {
        SceneManager.LoadScene(2);
    }
    public void Man3()
    {
        SceneManager.LoadScene(3);
    }
    public void Man4()
    {
        SceneManager.LoadScene(4);
    }
    public void Man5()
    {
        SceneManager.LoadScene(5);
    }
}
