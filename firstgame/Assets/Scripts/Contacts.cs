﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Contacts : MonoBehaviour
{
    private GameObject stick;
    private GameObject win;
    // public bool vacham;
   // public AudioSource aus;
    public AudioSource soundTrack;
    public AudioSource winSound;
    public AudioSource loseSound;
    public bool sound;

    void Start()
    {
        stick = new GameObject();
        win = new GameObject();
        
     //   vacham = false;
        stick = GameObject.FindGameObjectWithTag("UIManager");
        win = GameObject.FindGameObjectWithTag("WinTag");

        stick.SetActive(false);
        win.SetActive(false);
        Time.timeScale = 1;
        soundTrack.Play();
        sound = false;

    }

    private void Update()
    {
        if (gameObject.GetComponent<Touch>().touched && ! sound)
        {
            sound = true;
            if(gameObject.transform.position.z < 89 )
            {              
                stick.SetActive(true);
                soundTrack.Stop();
                loseSound.Play();
            }
            else
            {
                win.SetActive(true);
                soundTrack.Stop();
                winSound.Play();
            }
            Time.timeScale = 0;
        }
    }
    private void SoundMusic()
    {

    }
}
